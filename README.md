## This is Scripting The Idle Class, another web extension cheat engine for another incremental game, this time the cynical business sim The Idle Class.

So far, it's got these features:
* An auto-fire button in the acquisition page that calls 5 robots to smash that button as fast as they possibly can for you. You'll make so much acquisitions money you'll never see profit from anything else again.
* A resolve and destress button in the Outgoing Mail menu. Resolve carries out the action without sending the email, and without adding stress. Destress reduces stress amount.
* A sneaky button hidden in the STIC icon added near the main button. Automatically sets a robot to click away at the main earn button at the maximum click speed for that button (10 clicks/sec, 100ms per click).

Anyway, yeah, creating UI elements in js instead of cloning them like I was doing in MICE.

## Install add-on in Firefox or in Chrome
**Download the directory**. Either clone the repository or download and unzip into a new folder. 

In Firefox go to about:debugging, enable Add-on Debugging check box, and install as temporary addon by opening manifest.json.

In Chrome, go to settings dropdown > more tools > extensions, enable developer mode in the top right, and install unpacked extension by opening the folder that contains manifest.json.

***

Check out my blog-style page about The Idle Class console commands [here](https://youare.awsmppl.com/code).
